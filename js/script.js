

$(document).ready(function() {
   resizer();
   
   
   $(window).resize(function(){
      resizer();
   });

   try {
      $(window).bind('orientationchange', function () {
         resizer();
      });
   }catch(e){}



   $('input, textarea').focus(function(){
         var def = $(this).attr("defvalue")||'',
            val = $(this).val();
         if (def===val){
            $(this).val('');

         }
         $(this).addClass('with-val');
      });

   $('input, textarea').blur(function(){
      var def = $(this).attr("defvalue")||'',
         val = $(this).val();
      if (def===val || !val){
         $(this).val(def);
         $(this).removeClass('with-val');

      }
   });

   $('.phone-in').keydown(function(ev){
      if (ev.keyCode!==9 && (ev.keyCode<48 || ev.keyCode>57))
         if (ev.keyCode===8 || ev.keyCode===46 || ev.keyCode===37 || ev.keyCode===39 || ev.keyCode===36 || ev.keyCode===35 || ev.keyCode===107 || ev.keyCode===39){} else
            return false;
   });



   $('.button').click(function(ev){
      var f = $(this).closest("form");
      if (f.length>0)
         f.submit();
      else{
         $('.form').show();
      }
      ev.stopImmediatePropagation();
   });

   $('.form').click(function(ev){
      ev.stopImmediatePropagation();
   });

   $('body, .close').click(function(){
      $('.form').hide();
   });



   $(window).scroll(function(){
      onScroll();
   });

   onScroll();



});

function onScroll(){
   for (var i=3;i<11;i++){
      testThis(i);
   }

}

function testThis(ind){
   var st = $('body').scrollTop()||$(window).scrollTop();
   var sl = $('.sl'+String(ind));
   if (st>sl.offset().top-$(window).height()){
      sl.addClass('vis');
      if (ind===3){
         $('.sl4').addClass('vis');
      }
      if (ind===7){
         $('.sl8').addClass('vis');
         $('.sl9').addClass('vis');
      }
   }
}


function resizer(){
   setTimeout(function(){
      
      var w = $('body').width(),
         h = $(document).height();



      if (w<1220){

         $('body').removeClass('nolmalw');
         if (w<1000) {
            $('body').removeClass('w1024');
            $('body').addClass('w480');
            $('.diagramm img').attr("src", "img/diagramma480.jpg");
         } else {
            $('body').addClass('w1024');
            $('body').removeClass('w480');
            $('.diagramm img').attr("src", "img/diagramma.jpg");
         }
      } else {
         $('body').removeClass('w1024');
         $('body').removeClass('w480');
         $('body').addClass('nolmalw');
         $('.diagramm img').attr("src", "img/diagramma.jpg");
      }
   }, 1);
}
